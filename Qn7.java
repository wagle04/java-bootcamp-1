/*
7.	Write a program to find the perimeter of a circle, rectangle, and triangle.
*/


import java.util.Scanner;

public class Qn5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("1. Circle");
        System.out.println("2. Rectangle");
        System.out.println("3. Triangle");
        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();

        switch (choice) {
            case 1:
                System.out.print("Enter radius: ");
                float radius = sc.nextFloat();
                Circle c = new Circle(radius);
                System.out.println("Perimeter of circle: " + c.calculatePerimeter());
                break;
            case 2:
                System.out.print("Enter length: ");
                float length = sc.nextFloat();
                System.out.print("Enter breadth: ");
                float breadth = sc.nextFloat();
                Rectangle r = new Rectangle(length, breadth);
                System.out.println("Perimeter of rectangle: " + r.calculatePerimeter());
                break;
            case 3:
                System.out.print("Enter length of side 1: ");
                float side1 = sc.nextFloat();
                System.out.print("Enter length of side 2: ");
                float side2 = sc.nextFloat();
                System.out.print("Enter length of side 3: ");
                float side3 = sc.nextFloat();
                Triangle t = new Triangle(side1,side2,side3);
                System.out.println("Perimeter of triangle: " + t.calculatePerimeter());
                break;
            default:
                System.out.println("Only input of 1,2 and 3 is allowed");
        }

        sc.close();
    }
}

abstract class Shape {
     abstract float calculatePerimeter();
}


class Circle extends Shape {
    float radius;

    Circle(float radius) {
        this.radius = radius;
    }

    float calculatePerimeter() {
        return (float) (2* Math.PI * radius);
    }
}

class Rectangle extends Shape {
    float length;
    float breadth;

    Rectangle(float length, float breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    float calculatePerimeter() {
        return 2 * (length + breadth);
    }
}

class Triangle extends Shape {
    float side1;
    float side2;
    float side3;

    Triangle(float side1, float side2, float side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    float calculatePerimeter() {
        return side1 + side2 + side3;
    }
}