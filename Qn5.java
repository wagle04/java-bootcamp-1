/*
5.	Write a program to find the area of a circle, rectangle, and triangle.
*/


import java.util.Scanner;

public class Qn5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("1. Circle");
        System.out.println("2. Rectangle");
        System.out.println("3. Triangle");
        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();

        switch (choice) {
            case 1:
                System.out.print("Enter radius: ");
                float radius = sc.nextFloat();
                Circle c = new Circle(radius);
                System.out.println("Area of circle: " + c.calculateArea());
                break;
            case 2:
                System.out.print("Enter length: ");
                float length = sc.nextFloat();
                System.out.print("Enter breadth: ");
                float breadth = sc.nextFloat();
                Rectangle r = new Rectangle(length, breadth);
                System.out.println("Area of rectangle: " + r.calculateArea());
                break;
            case 3:
                System.out.print("Enter base: ");
                float base = sc.nextFloat();
                System.out.print("Enter height: ");
                float height = sc.nextFloat();
                Triangle t = new Triangle(base, height);
                System.out.println("Area of triangle: " + t.calculateArea());
                break;
            default:
                System.out.println("Only input of 1,2 and 3 is allowed");
        }

        sc.close();
    }
}

abstract class Shape {
     abstract float calculateArea();
}


class Circle extends Shape {
    float radius;

    Circle(float radius) {
        this.radius = radius;
    }

    float calculateArea() {
        return (float) (Math.PI * radius * radius);
    }
}

class Rectangle extends Shape {
    float length;
    float breadth;

    Rectangle(float length, float breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    float calculateArea() {
        return length * breadth;
    }
}

class Triangle extends Shape {
    float base;
    float height;

    Triangle(float base, float height) {
        this.base = base;
        this.height = height;
    }

    float calculateArea() {
        return (base * height) / 2;
    }
}