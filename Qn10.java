/*

10.	Write a pro Write a Program for the following Mathematical Function.
a.	 s= ut+ ½ at2
b.	 area= sqrt(s(s-a)(s-b)(s-c))
c.	 x= (-b+ sqrt(b2-4ac))/2a

*/


import java.util.Scanner;

public class Qn10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("1. s= ut+ ½ at2");
        System.out.println("2. area= sqrt(s(s-a)(s-b)(s-c))");
        System.out.println("3. x= (-b+ sqrt(b2-4ac))/2a");
        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();

        switch (choice) {
            case 1:
                System.out.print("Enter u: ");
                float u = sc.nextFloat();
                System.out.print("Enter t: ");
                float t = sc.nextFloat();
                System.out.print("Enter a: ");
                float a = sc.nextFloat();
                float s = (u * t) + (0.5f * a * t * t);
                System.out.println("s= " + s);
                break;
            case 2:
                System.out.print("Enter a: ");
                float a2 = sc.nextFloat();
                System.out.print("Enter b: ");
                float b = sc.nextFloat();
                System.out.print("Enter c: ");
                float c = sc.nextFloat();
                float s2 = (a2 + b + c) / 2;
                float area = (float) Math.sqrt(s2 * (s2 - a2) * (s2 - b) * (s2 - c));
                System.out.println("area= " + area);
                break;
            case 3:
                System.out.print("Enter a: ");
                float a3 = sc.nextFloat();
                System.out.print("Enter b: ");
                float b2 = sc.nextFloat();
                System.out.print("Enter c: ");
                float c2 = sc.nextFloat();
                float x = (float) ((-b2 + Math.sqrt(b2 * b2 - (4 * a3 * c2))) / (2 * a3));
                System.out.println("x= " + x);
                break;
            default:
                System.out.println("Only input of 1, 2 and 3 is allowed");
        }

        sc.close();
    }
}

