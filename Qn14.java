/*
14.	Write a program to relate two integers entered by the user using = =or > or < sign.

*/



import java.util.Scanner;


public class Qn14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first number: ");
        int num1 = sc.nextInt();

        System.out.print("Enter second number: ");
        int num2 = sc.nextInt();

        if(num1 == num2){
            System.out.println("Both numbers are equal");
        }else if(num1 > num2){
            System.out.println("First number is greater than second number");
        }else{
            System.out.println("Second number is greater than first number");
        }

        sc.close();
    }
}