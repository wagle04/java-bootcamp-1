/*
4.	Write a program to calculate Simple Interest input by the user. Simple Interest = P*T*R/100
*/

import java.util.Scanner;

public class Qn4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter principal amount: ");
        float principal = sc.nextFloat();

        System.out.print("Enter rate of interest: ");
        float rate = sc.nextFloat();

        System.out.print("Enter time period (in years): ");
        float time = sc.nextFloat();

        float simpleInterest = (principal * rate * time) / 100;

        System.out.println("Simple Interest= " + simpleInterest);

        sc.close();
    }
}