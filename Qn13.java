/*

13.	Write a program to print the number entered by the user only if the number entered is negative.
*/



import java.util.Scanner;

public class Qn13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = sc.nextInt();

        if (num < 0) {
            System.out.println("The number you entered is: " + num);
        }

        sc.close();
    }
}