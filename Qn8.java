/*
8.	Write a program that reads the radius and length of a cylinder and computes volume.
*/

import java.util.Scanner;

public class Qn8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter radius: ");
        float radius = sc.nextFloat();
        System.out.print("Enter length: ");
        float length = sc.nextFloat();

        Cylinder c = new Cylinder(radius, length);
        float volume = c.calculateVolume();

        System.out.println("Volume of cylinder: " + volume);

        sc.close();
    }
}

class Cylinder {
    float radius;
    float length;

    Cylinder(float radius, float length) {
        this.radius = radius;
        this.length = length;
    }

    float calculateVolume() {
        return (float) (Math.PI * Math.pow(radius, 2) * length);
    }
}