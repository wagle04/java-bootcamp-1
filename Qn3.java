/*
3.	Write a program to find sum and average of two numbers input by User (using Scanner class).
*/

import java.util.Scanner;

public class Qn3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first number: ");
        float num1 = sc.nextFloat();

        System.out.print("Enter second number: ");
        float num2 = sc.nextFloat();

        float sum = num1 + num2;
        float avg = sum / 2;

        System.out.println("Sum: " + sum);
        System.out.println("Average: " + avg);
        
        sc.close();
    }
}