/*
9.	Write a program to convert Fahrenheit to Celsius and Celsius to Fahrenheit.

*/

import java.util.Scanner;

public class Qn9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("1. Fahrenheit to Celsius");
        System.out.println("2. Celsius to Fahrenheit");
        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();

        switch (choice) {
            case 1:
                System.out.print("Enter temperature in Fahrenheit: ");
                float fahrenheit = sc.nextFloat();
                float celsius = (fahrenheit - 32) * 5 / 9;
                System.out.println("Temperature in Celsius: " + celsius);
                break;
            case 2:
                System.out.print("Enter temperature in Celsius: ");
                float celsius2 = sc.nextFloat();
                float fahrenheit2 = (celsius2 * 9 / 5) + 32;
                System.out.println("Temperature in Fahrenheit: " + fahrenheit2);
                break;
            default:
                System.out.println("Only input of 1 and 2 is allowed");
        }

        sc.close();
    }
}