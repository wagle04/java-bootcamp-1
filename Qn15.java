/*

15.	Write a program that receives an ASCII code (between 0 – 128) and display its character [example: 97 (input) ->a(output)].
*/


import java.util.Scanner;


public class Qn15{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the ASCII code: ");
        int ascii = sc.nextInt();
        char ch = (char)ascii;
        System.out.println("The character is: "+ch);
    }
}