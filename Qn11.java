/*
11.	Write a program to swap two numbers
a) using temp variable
b) without temp variable

*/

import java.util.Scanner;

public class Qn11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first number: ");
        float num1 = sc.nextFloat();
        float num12 = num1;
        System.out.print("Enter second number: ");
        float num2 = sc.nextFloat();
        float num22 = num2;


        // Swapping using temp variable
        float temp = num1;
        num1 = num2;
        num2 = temp;

        System.out.println("After swapping using temp variable:");
        System.out.println("First number: " + num1);
        System.out.println("Second number: " + num2);

        // Swapping without using temp variable
        num12 = num12 + num22;
        num22 = num12 - num22;
        num12 = num12 - num22;

        System.out.println("After swapping without using temp variable:");
        System.out.println("First number: " + num12);
        System.out.println("Second number: " + num22);

        sc.close();
    }
}