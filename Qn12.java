/*
12.	Write a program to input the roll, name, and nationality of the person and display those values in good format.
*/


import java.util.Scanner;


public class Qn12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter roll: ");
        int roll = sc.nextInt();

        System.out.print("Enter name: ");
        String name = sc.next();

        System.out.print("Enter nationality: ");
        String nationality = sc.next();
        
        Person p = new Person(name,nationality,roll);

        p.displayDetail();

        sc.close();
    }
}


class Person{

    String name;
    String nationality;
    int roll;

    Person(String name,String nationality,int roll){
        this.name = name;
        this.roll = roll;
        this.nationality = nationality;
    }

    void displayDetail(){
        System.out.println("\n\nName: " + name + "\nRoll: "+ roll+"\nNationality: "+nationality);
    }

}