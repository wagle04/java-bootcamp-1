/*
6.	Write a program that converts pounds into kg. The program prompts the user to enter a number of pounds, converts it to kg and displays the result [1 pound is 0.454 kg].
*/


import java.util.Scanner;

public class Qn6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter weight in pounds: ");
        float pounds = sc.nextFloat();

        float kg = pounds * 0.454f;

        System.out.println("Weight in kg: " + kg);

        sc.close();
    }
}